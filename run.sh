#!/usr/bin/bash

docker run -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix -v $XAUTHORITY:$HOME/.Xauthority -v $HOME/.Xauthority:/home/wineuser/.Xauthority \
	-v $(pwd)/home:/home/wineuser \
	--network=host \
	--gpus='all,"capabilities=compute,utility,graphics,display"' \
	--rm -it nvwx:latest cinit "$@"
