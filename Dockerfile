FROM nvidia/cudagl:11.4.0-runtime-ubuntu20.04

ARG WINE_BRANCH=devel
ARG DXVK_VERSION=1.9.4

ARG DEBIAN_FRONTEND=noninteractive

# nvidia-prime
ENV  __NV_PRIME_RENDER_OFFLOAD=1
ENV __GLX_VENDOR_LIBRARY_NAME=nvidia
ENV __VK_LAYER_NV_optimus=NVIDIA_only

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install -y libvulkan1 vulkan-tools software-properties-common mesa-utils nano wget pulseaudio-utils iputils-ping winbind ca-certificates cabextract

# install wine
RUN dpkg --add-architecture i386
RUN wget -nc https://dl.winehq.org/wine-builds/winehq.key
RUN apt-key add winehq.key
RUN add-apt-repository 'deb https://dl.winehq.org/wine-builds/ubuntu/ focal main'
RUN apt-get update
RUN apt-get install -y --install-recommends winehq-$WINE_BRANCH

# install winetricks
RUN wget -O /usr/bin/winetricks https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks
RUN chmod +x /usr/bin/winetricks

# download gecko and mono installers
COPY download_gecko_and_mono.sh /tmp/download_gecko_and_mono.sh
RUN chmod +x /tmp/download_gecko_and_mono.sh
RUN  /tmp/download_gecko_and_mono.sh "$(dpkg -s wine-$WINE_BRANCH | grep "^Version:\s" | awk '{print $2}' | sed -E 's/~.*$//')"

RUN groupadd wineuser -g 1000
RUN useradd --uid 1000 --gid 1000 -m --shell /bin/bash wineuser
RUN usermod -a -G audio wineuser

COPY nvidia_icd.json /etc/vulkan/icd.d/nvidia_icd.json
COPY pulse-client.conf /etc/pulse/client.conf
COPY cinit.sh /usr/bin/cinit

RUN wget -O /tmp/dxvk-$DXVK_VERSION.tar.gz https://github.com/doitsujin/dxvk/releases/download/v$DXVK_VERSION/dxvk-$DXVK_VERSION.tar.gz
RUN mkdir /opt/dxvk
RUN tar -xf /tmp/dxvk-$DXVK_VERSION.tar.gz --strip-components=1 -C /opt/dxvk
RUN rm /tmp/dxvk-$DXVK_VERSION.tar.gz

RUN setcap cap_net_raw+epi "/opt/wine-$WINE_BRANCH/bin/wine64-preloader"
RUN setcap cap_net_raw+epi "/opt/wine-$WINE_BRANCH/bin/wine-preloader"

RUN apt-get clean

USER wineuser
WORKDIR /home/wineuser

CMD ["cinit"]
